# P1 answer

## Result Comments

Comment on the differences between your `p1_results` and the ones given for this practical.

Mon code était sensiblement le même, donc j'ai gardé le travail que j'avais fait moi-même

===============================================

# P2 answer

## Result Comments

Are your results identical to the ones demanded in **practical 2**? If not, why?

Les résultats sont corrects uniquement pour la première trajectoire. 
J'obtiens une réponse pour un deltat = 2s
Par contre dès la 2e trajactoire, chaque réponses s'effectuent instantanément. Le deltat n'est plus pris en compte. 
Exemple: x_initial = 1 et y_initial = 2 et deltat = 2
Le courbeva directement de 1 à 2 et ignore le deltat.

==============================================

# P3 answer

## Merge Comments

J'ai préféré garder ma version de code.. même si votre solution semble plus simple et compréhensible

## Result Comments

J'ai réussi a faire fonctionner avec mon code comme c'est montré dans le GIF.

===============================================

# P4 answer

## Merge Comments

TP non fait.

===============================================

# P5 answer

## Merge and Results Comments

Après avoir regardé votre code, j'ai bien calculé la nouvelle position à chaques fois, du moins avec la bonne formule.
J'ai print cette position.
Cependant après avoir lancé le bash_script, je me suis rendu compte que la nouvelle position calculée restait toujours à 0 , 0.
C'est pourquoi je n'arrivais pas à avoir une trajectoire asservie en fonction de la position désirée car j'avais un problème au niveau de ma position dans le Simulateur. Je pensais que j'avais un problème avec mon Simulateur.

J'ai repris votre programme dans mon projet.

===============================================

# P6 answer


## Result Comments

Pour ce TP j'ai réussi à creer des perturbations et les inclure dans le simulateur.
J'ai réussi à faire le launch.py, cependant j'ai repris sur internet les noeuds robot_state_publisher et static_transform_publisher car je n'ai rien compris à leur fonctionnement ...
En finalité j'ai réussi à tracer sur rviz et sur plotjuggler.