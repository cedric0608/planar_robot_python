import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
from custom_messages.msg import KinData, CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class Simulator(Node):
    def __init__(self):
        super().__init__('simulator')
        self.subscriber1 = self.create_subscription(JointState, 'desired_joint_velocities', self.listener_callback, 10)
        self.publisher = self.create_publisher(JointState, 'joint_states', 10)

        self.q_previous = np.zeros(2)
        self.joint_velocity = np.zeros(2)
        self.sampling_period = 0.1
        self.test = False
        
        # un timer est utilisé pour recalculer en permanence la nouvelle position.
        # il ne faut pas calculer la nouvelle position uniquement à la réception de la vélocité, sinon le robot ferait comme des à-coups
        self.run_timer = self.create_timer(self.sampling_period, self.timer_callback)
        
    def listener_callback(self, msg):
        
        self.joint_velocity = msg.velocity
        self.joint_position = msg.position

        if self.test == False:
            self.q_previous = self.joint_position
            self.test == True

    
    def timer_callback(self):
        
        """ # Calcul du vecteur q_désiré
        qd = qd_dot * 0.1 + self.qd_previous
        self.qd_previous = qd """

        self.q = self.q_previous * self.sampling_period + self.joint_velocity
        self.q_previous = self.q
        
        #self.q_memory = self.q_memory + self.sampling_period * self.joint_velocity

        message = JointState()
        message.position = self.q
        print("q = ", self.q)

        self.publisher.publish(message)
        
def main(args=None):
    rclpy.init(args=args)
    simulator = Simulator()
    rclpy.spin(simulator)
    simulator.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
