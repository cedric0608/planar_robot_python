import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool
from custom_messages.msg import KinData
from custom_messages.msg import RefPosition
import numpy as np  
import csv

class High_Level_Manager(Node):
    def __init__(self):
        super().__init__('high_level_manager')
        # Création des éditeurs et des abonnés
        self.publisher = self.create_publisher(RefPosition, 'ref_position', 10)
        self.subscriber1 = self.create_subscription(KinData, 'kin_data', self.KinData_listener_callback, 10)
        self.subscriber2 = self.create_subscription(Bool, 'launch', self.launch_listener_callback, 10)

        # Initialisation des variables
        self.initialized = False
        self.running = False
        self.trajectory = None
        self.CSV_line = 0
        self.tolerance = 1/100 

    def load_trajectory_from_csv(self, file_path):
        # Lecture du fichier CSV et chargement de la trajectoire
        with open(file_path, 'r') as csvfile:
            csv_reader = csv.reader(csvfile)
            next(csv_reader)  # Ignorer la première ligne qui est souvent des en-têtes
            trajectory = []
            for row in csv_reader:
                values = [float(value) for value in row]  # Conversion des valeurs en float
                trajectory.append(values)
                print(f"Loaded positions {values}")
            return trajectory

    def KinData_listener_callback(self, msg):
        if self.trajectory is None:
            return

        message = RefPosition()
        if not self.initialized:
            # Si le système n'est pas initialisé, publier la première position reçue
            message.x = msg.x
            message.y = msg.y
            message.deltat = 0.0
            self.initialized = True
            self.publisher.publish(message)
        elif self.running:
            # Si le système est en cours d'exécution, vérifier si la position actuelle est proche de la position cible
            if self.CSV_line < len(self.trajectory):
                target_x, target_y, target_deltat = self.trajectory[self.CSV_line]
                if abs(msg.x - target_x) < self.tolerance and abs(msg.y - target_y) < self.tolerance:
                    message.x = target_x
                    message.y = target_y
                    message.deltat = target_deltat
                    self.publisher.publish(message)
                    self.CSV_line += 1
            else:
                # Si la trajectoire est terminée, arrêter le système
                self.running = False

    def launch_listener_callback(self, msg):
        if msg.data and not self.running:
            # Si le message de lancement est reçu et le système n'est pas déjà en cours d'exécution
            self.running = True
            if self.trajectory:
                # Si une trajectoire est chargée, publier la première position de la trajectoire
                target_x, target_y, target_deltat = self.trajectory[self.CSV_line]
                message = RefPosition()
                message.x = target_x
                message.y = target_y
                message.deltat = target_deltat
                self.publisher.publish(message)

def main(args=None):
    rclpy.init(args=args)
    high_level_manager = High_Level_Manager()
    # Chargement de la trajectoire à partir du fichier CSV
    file_path = '/home/cedric0608/TP_ros2/src/planar_robot_python/trajectories/test.csv'
    high_level_manager.trajectory = high_level_manager.load_trajectory_from_csv(file_path)
    if high_level_manager.trajectory:
        # Si la trajectoire est chargée avec succès, démarrer le nœud
        rclpy.spin(high_level_manager)
    high_level_manager.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
