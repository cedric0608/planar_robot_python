import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32
from custom_messages.msg import CartesianState  
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class TrajectorySample(Node):
    def __init__(self):
        super().__init__('trajectory_sample')

        #       - Initialize node to be publisher and subscriber to the necessary topics
        
        self.subscriber_ = self.create_subscription(Float32,'time',self.callback,10) # on écoute sur le topic time
        # callback est appellée dès qu'un message est reçu dans time (topic)
        self.publisher_ = self.create_publisher(CartesianState,'desired_state',10) # on publie sur le topic desired_state

        #       - Initialize main variables

        pi                  = np.array([1.0,2.0]) # position initiale
        pf                  = np.array([3.0,4.0]) # position finale
        dt                  = 2.0                 # intervalle de temps
        self.pos_interp     = PositionInterpolation(pi,pf,dt) # on initialise la classe PositionInterpolation

    #       - Create callback function able to compute the position and velocity for a given instant t

    def callback(self,msg):    

        # on récupère les positions et les vitesses à l'instant t de la classe 'PositionInterpolation'
        t = msg.data
        position = self.pos_interp.p(t)
        velocity = self.pos_interp.pdot(t)

        # on crée un message de type CartesianState qui contient x, y , xdot, ydot
        message = CartesianState()

        message.x = position[0] 
        message.y = position[1]
        message.xdot = velocity[0]
        message.ydot = velocity[1]

        # apres le cacul, il faut published dans desired _state
        self.publisher_.publish(message)
        self
        # On crée les variables x, y, xdot et ydot à afficher et on y affecte les valeurs contenuent dans position et velocity
        self.get_logger().info('Publishing: x: {x}, y: {y}, xdot: {xdot}, ydot: {ydot}'.format(x=position[0], y=position[1], xdot=velocity[0], ydot=velocity[1]))

def main(args=None):
    rclpy.init(args=args)
    trajectory_sample = TrajectorySample()
    rclpy.spin(trajectory_sample)
    trajectory_sample.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()