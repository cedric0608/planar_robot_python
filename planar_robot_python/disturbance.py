import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
import math
import numpy as np

class Disturbance(Node):
    def __init__(self):
        super().__init__('disturbance')
        self.publisher = self.create_publisher(JointState, '/qdot_disturbance', 10)

        self.sampling_period = 0.1
        self.timer = self.create_timer(self.sampling_period, self.publish_disturbance)  # Adjust the frequency as needed

        # Parameters
        self.declare_parameter('amplitude', 1)
        self.declare_parameter('omega', 100)

        self.amplitude = self.get_parameter('amplitude').value
        self.omega = self.get_parameter('omega').value
        self.dt = 0.0
        self.q_dot = np.zeros(2)

    def publish_disturbance(self):
        
        self.dt += self.dt + self.sampling_period

        self.q_dot[0] = self.amplitude * math.sin(self.omega * self.dt)
        self.q_dot[1] = 2*self.amplitude * math.sin((self.omega)/2 * self.dt)

        msg = JointState()
        msg.velocity = self.q_dot
        self.publisher.publish(msg)

def main(args=None):
    rclpy.init(args=args)
    disturbance = Disturbance()
    rclpy.spin(disturbance)
    disturbance.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()